import Layout from '../Comps/Layout'
import '../styles/globals.scss'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'antd/dist/antd.css';
import { appWithTranslation }   from '../i18n'

function MyApp({ Component, pageProps }) {
  return (
    <Layout>
      <Component {...pageProps}/>
    </Layout>
  )
}

export default appWithTranslation(MyApp);
